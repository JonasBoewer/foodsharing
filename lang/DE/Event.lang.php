<?php
global $g_lang;

$g_lang['new_event'] = 'Neuen Termin eintragen';
$g_lang['no_events_posted'] = 'Noch keine Events vorhanden';
$g_lang['new_event'] = 'Neue/n Termin/Veranstaltung eintragen';
$g_lang['description'] = 'Beschreibung';
$g_lang['desc_desc'] = 'Was ist das für ein Event?';
$g_lang['location_name'] = 'Veranstaltungsort';
$g_lang['online_type'] = 'Findet das Event offline oder online auf unserem Mumbleserver statt?';
$g_lang['offline'] = 'Ganz normal im echten Leben';
$g_lang['online'] = 'Multilokal auf mumble.foodsharing.de';
$g_lang['mumble_room'] = 'In welchem mumble-Konferenzraum treffen wir uns?';
$g_lang['dateend'] = 'Enddatum';
$g_lang['event_options'] = 'Eventoptionen';
$g_lang['saveEventInfo'] = '<b>Bitte beachten:</b><br>Beim Speichern werden alle derzeitigen Mitglieder der oben gewählten Gruppe eingeladen. Um Neuzugänge in Bezirk oder AG später noch dazuzuladen, speichere den Termin einfach erneut.';
$g_lang['position_search_infobox'] = 'Bitte gib die Adresse des Treffpunktes im Textfeld zwischen dieser Nachricht und der Karte ein und bestätige diese dann durch Auswahl in der Karte.';

